function numbersOutput(number)
{
    if (!number) {
        return 'Empty param!'
    }

    if (typeof number !== 'number') {
        return 'Type Error!'
    }

    let list = ''

    for (let i = 1; i <= number; i++) {
        switch (i) {
            case 5:
                list += 'пять '
                break
            case 13:
                list += 'тринадцать '
                break
            case 22:
                list += 'двадцать два '
                break
            case 35:
                list += 'тридцать пять '
                break
            case 98:
                list += 'девяносто восемь '
                break
            default:
                list += i + ' '
                break
        }
    }

    return list
}

console.log(numbersOutput(15))
